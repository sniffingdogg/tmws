<?php 
/************************************************************************************

class buildTable July 2014  SNIFFINGDOGG

Gets qualified records for notification, builds an HTML table


************************************************************************************/

class buildTable {

private $tget;
private $tstamp;
private $spd;
private $uid;
private $name;

function __construct() {

    date_default_timezone_set('America/Los_Angeles');
	
} //end __construct


public function displayClocks($array) {

	$htmlTableClock = "";
	$rows = 0;
		
    foreach ($array as  $vclocks) {
      	
      		foreach ($vclocks as $key=>$value) {
      			if ($key == 'username') {
      				$this->tget =  $value;
      			}elseif ($key == 'groupname') {
      				$this->tget =  $value;
      			}elseif ($key == 'timestamp') {
      				$date = new DateTime($value);
      				$this->tstamp = $date->format('Y-m-d H:i:s');
      			}elseif ($key == 'speed') {
      				$this->spd = $value;
      			}else
      				error_log("none");
      		} // end for
				$rows++;
	            $htmlTableClock .= '
                        <tr>
                            <td class="uid" name="uid' . $rows . '" id="uid">' . $this->tget . '</td>
                            <td> ' . $this->tstamp . '</td>
                            <td> ' . $this->spd . ' </td>
	                        <td> <input type="button" class="tdsmall deleteRow" id="deleteRow"> </td>
                        </tr> ';
	  
	} // end for

	return $htmlTableClock;

} // end displayClocks

public function displayUsers($array) {

	$htmlTableUsers = "";
        $rows = 0;
		
    foreach ($array as  $vclocks) {
      	
      		foreach ($vclocks as $key=>$value) {
      			if ($key == 'ID') {
      				$this->uid =  $value;
      			}elseif ($key == 'Name') {
      				$this->name = $value;
      			}else
      				error_log("none");
      		} // end for
                                $rows++;
	            $htmlTableUsers .= '
                        <tr>
                            <td class="uid" name="uid' . $rows . '" id="uid">' . $this->uid . '</td>
                            <td> ' . $this->name . '</td>
	                        <td> <input type="button" class="tdsmall addClock" id="addClock"> </td>
                        </tr> ';
	  
	} // end for

	return $htmlTableUsers;

} // end displayUsers


} // end class