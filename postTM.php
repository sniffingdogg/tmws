<?php 
/************************************************************************************

postTM Jul 2014  SNIFFINGDOGG

This program is called from AJAX and routes data from viewTM for evaluation and/or update

************************************************************************************/

require_once("getTM.php");

$getTM = new getTM;

if ($_SERVER["REQUEST_METHOD"] == "POST")
    $tab = $_POST["tab"];
    


if ($tab == 'aOdata_delete') {
	$deleteRow = $_POST["row"];
//	$v1 = 'uid' . $deleteRow;
//  $uid = $_POST[$v1];
    		
    $getTM->remClock("username",$deleteRow);
    		
	echo 'pass';
	
}// end aOdata_delete

if ($tab == 'addClock') {
	$uid = $_POST["acuid"];
	$absClock = $_POST["absClock"];
	$clockSpeed = $_POST["clockSpeed"];
        
        $absClock = str_replace('/','',$absClock);
        $absClock = str_replace(' ','',$absClock);
        $absClock = str_replace(':','',$absClock);  

        $getTM->setAbsClock($uid,$absClock,$clockSpeed);
    		
	echo 'pass';
	
}// end addClock

?>