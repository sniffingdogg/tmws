<?php 
/************************************************************************************

viewTM July 2014  SNIFFINGDOGG

Handle view for TM interaction.

************************************************************************************/

require_once("getTM.php");
require_once("buildTable.class.php");

$buildTable = new buildTable;
$getTM = new getTM;
$htmlTableUser = 'none';
$htmlTableClock = 'none';

if (!$vclocks = $getTM->getClocks())
	error_log("viewTM: Bad Return from TMAPP List Clocks");
else
	$htmlTableClock = $buildTable->displayClocks($vclocks);
error_log('htmlClock:' . $htmlTableClock);
if (!$vclocks = $getTM->getUsers())
	error_log("viewTM: Bad Return from TMAPP Get Users");
else
	$htmlTableUser = $buildTable->displayUsers($vclocks);
error_log('htmlUser:' . $htmlTableUser);
buildPage($htmlTableClock,$htmlTableUser);
		

function buildPage($htmlTableClock,$htmlTableUser) {

	$pageTitle = "TimeMachine";
	$section = "Notify";
	$html = "";

	include('header.php'); 
	
$html .= '
	<div class="section page">
	<div class="wrapper">
	<div id="formwrapper">
 <form method="post" action="#" class="form_notified" id="tmForm">
	<div class="tabbedPanels" id="tabbedPanels">
		<ul class="tabs">
			<li><a href="#panel1" tabindex="1">List Clocks</a></li>
  			<li><a href="#panel2" tabindex="2">List Users</a></li>
  			<li><a href="#panel3" tabindex="3">Add Virtual Clock</a></li>
		</ul>
	<div class="panelContainer">
	<div id="panel1" class="panel">
            <div style="width:99%; overflow:hidden; height:100%; overflow-x:scroll;">
                <div class="table_container">
            	<table id="aoTable" class="tablesorter">
				<thead>
					<tr>
					<th>Target</th>
					<th>Timestamp</th>
					<th>Speed</th>
					<th class="tdsmall">Delete</th>
					</tr>
				</thead>
				<tbody>';
            	$html .= $htmlTableClock . '</tbody> </table>
            </div>
            </div>
	</div>

	<div id="panel2" class="panel">
            <div style="width:99%; overflow:hidden; height:100%; overflow-x:scroll;">
                <div class="table_container">
            	<table id="userTable" class="tablesorter">
				<thead>
					<tr>
					<th>ID</th>
					<th>Name</th>
					<th class="tdsmall">New Clock</th>
					</tr>
				</thead>
			<tbody>';
                            $html .= $htmlTableUser . '</tbody> </table>
                </div>
            </div>
	</div>
	<div id="panel3" class="panel">
            <div style="width:99%; overflow:hidden; height:100%; overflow-x:scroll;">
                <div class="table_container">
            	<table id="clockTable" class="tablesorter">
                    <thead>
			<tr>
			<th>UID</th>
			<th>Absolute Clock</th>
                        <th>Clock Speed</th>
                        <th>Set Clock</th>
			</tr>
                    </thead>
		<tbody>
                    <tr>
                        <td> <input type="text" class="required acuid" name="acuid" id="acuid" value="500"> </td>
                        <td> <input type="text" class="absClock" name="absClock" id="datetimepicker" > </td>
                        <td> <input type="number" class="required clockSpeed" name="clockSpeed" id="clockSpeed" value=1> </td>
                        <td> <input type="button" id="acSubmit" value="Add Abs Clock" class="acSubmit"> </td>
                    </tr>
                </tbody> 
                </table>
            </div>
            </div>
			</div>
		</div>
	</div>
	</div> 
	</form>
	</div>';
	
	echo $html;

?>	
<script>
	$(document).ready(function() {

	$('.deleteRow').css('background', "url(img/DeleteRed.png)");
	$('.addClock').css('background', "url(img/clock2.png)");
        $('#datetimepicker').datetimepicker();
//        var $tabs = $('.tabs').tabs();


/*  The Tabbed panels script starts here */
	$('.tabs a').click(function() {
		// save $(this) in a variable for efficiency
		var $this = $(this);
		
		// hide panels
		$('.panel').hide();
		$('.tabs a.active').removeClass('active');
		    
		// add active state to new tab
		$this.addClass('active').blur();	
		// retrieve href from link (is id of panel to display)
		var panel = $this.attr('href');
		// show panel
		$(panel).fadeIn(250);
		
		// don't follow link down page
		return(false);
	}); // end click
	 
	// open first tab
/*	$('.tabs li:first a').click();
*/	$('.tabs li:eq(0) a').click();

/* The sticky header for the tables starts here */
	$(function(){
		var options = {
	    widthFixed : true,
	    showProcessing: true,
		headerTemplate : '{content} {icon}', // Add icon for jui theme; new in v2.7!
		widgets: [ 'uitheme', 'zebra', 'stickyHeaders', 'filter' ],
		widgetOptions: {
      // extra class name added to the sticky header row
		    stickyHeaders : '',
      // number or jquery selector targeting the position:fixed element
      		stickyHeaders_offset : 0,
      // added to table ID, if it exists
      		stickyHeaders_cloneId : '-sticky',
      // trigger "resize" event on headers
      		stickyHeaders_addResizeEvent : true,
      // if false and a caption exist, it won't be included in the sticky header
      		stickyHeaders_includeCaption : true,
      // The zIndex of the stickyHeaders, allows the user to adjust this to their needs
      		stickyHeaders_zIndex : 2,
      // jQuery selector or object to attach sticky header to
      		stickyHeaders_attachTo : $('.table_container'),
		} // end var options
	  }; // end widget options

  	$("#aoTable").tablesorter(options);
//  	$("#clockTable").tablesorter(options);
  	$("#userTable").tablesorter(options);

	});  //end sticky header anonymous function


/* Function for virtual clock delete */
	$('.deleteRow').click(function() {
		var id = $(this).closest("tr").find(".uid").text();
		var aOdata = 'tab=aOdata_delete&row=' + id;
		var jqXHR = $.post('postTM.php',aOdata,processDelete).error('ouch');
		function processDelete(data) {
	        if (data=='pass') {
				$(location).attr('href','viewTM.php?bat=0');
		    } else {
        	    $('#formwrapper').prepend('<p id="fail">Invalid Input Data. Please try again ' + data + '</p>');
          		}		
		} //end processDelete
			
		return false;

	}); // end click

/* Function for virtual clock add */
	$('.addClock').click(function() {
		var id = $(this).closest("tr").find(".uid").text();
		var aOdata = 'tab=aOdata_add&row=' + id;
                $('.tabs li:eq(2) a').click();
		$('#acuid').val(id);
            
		return false;

	}); // end click

/* Function for actually adding the clock */
	$('#acSubmit').click(function() {
		var aBdata = 'tab=addClock&' + $('#clockTable :input').serialize();
		var jqXHR = $.post('postTM.php',aBdata,processData).error('ouch');
		$('.tabbedPanels').css('visibility', 'hidden');
		$('#img_animate').fadeIn();
		$('#img_animate').css({
				'position' : 'absolute',
				'margin' : 'auto'
		});

		function processData(data) {
	        if (data=='pass') {
				$(location).attr('href','viewTM.php');
		    } else {
        	    $('#formwrapper').prepend('<p id="fail">Invalid Input Data. Please try again ' + data + '</p>');
          		}		
		} //end processData
			
		return false;

	}); // end click


	}); // end document ready
</script>
	
<?php	include('footer.php');

} // end _buildPage

?>