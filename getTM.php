<?php
/************************************************************************************

class getTM January 2014  SNIFFINGDOGG

This makes the call to TM web service and display JSON


************************************************************************************/


class getTM {


function __construct() {

	

} //end __construct


public function getClocks() {

	$parsed_json = $this->callTM("listclock");
    return $parsed_json;
 
} // end getClocks

public function getUsers() {

	$parsed_json = $this->callTM("getusers");
    return $parsed_json;
 
} // end getClocks

public function getGroups() {

	$parsed_json = $this->callTM("getgroups");
    return $parsed_json;
 
} // end getClocks

public function setAbsClock($tmuser,$vclock,$spd) {

	$parsed_json = $this->callTM("addabsclock?userid=" . $tmuser . "&timestamp=" . $vclock . "&speed=" . $spd);
    return $parsed_json;
 
} // end getClocks

public function setRelClock($tmuser,$vclock,$spd) {

	$parsed_json = $this->callTM("addrelclock?userid=" . $tmuser . "&timestamp=" . $vclock . "&speed=" . $spd);
    return $parsed_json;
 
} // end getClocks

public function remClock($tget,$uid) {

	$parsed_json = $this->callTM("removeclock?" . $tget . "=" . $uid);
    return $parsed_json;
 
} // end getClocks

function callTM($params) {

    $url = "http://192.168.20.122:8800/tmapp/" . $params;
    $ch = curl_init();
    error_log("target:".$url);
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PORT, 8800);
 //   curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
 //   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    if (!$response = curl_exec($ch)) {
    	error_log("FAILED!! " . curl_error($ch));
    	return $response;
	}else {
    	$parsed_json = json_decode($response, true);
    	return $parsed_json;
		}
	
			
} // end callTM

} // end class

?>