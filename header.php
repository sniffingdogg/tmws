<?php 
/************************************************************************************

header January 2014  SNIFFINGDOGG

This is the HTML Header for all pages


************************************************************************************/
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $pageTitle; ?></title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
	<link rel="stylesheet" href="css/theme.default.css">
	<link rel="stylesheet" href="css/jquery.datetimepicker.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Rock+Salt|Oswald:400,700|Share" type="text/css">
	<link rel="shortcut icon" href="books.ico">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="js/jquery.tablesorter.js"></script>
	<script src="js/jquery.tablesorter.widgets.js"></script>    
	<script src="js/jquery.datetimepicker.js"></script>
	<script src="js/jquery.validate.min.js"></script>
	<script>
	$(document).ready(function(){
    	var d = new Date();
		var h = d.getHours();
		var bg;
		var bb;

		if (h >= 5 && h <= 12) {
			bg = '#A5BA2E';
			bb = '#2F583B';
		}
		if (h >= 13 && h <= 18) {
			bg = '#ff8400';
			bb = '#f16702';
		}
		if (h >= 19 && h <= 24) {
			bg = '#005CD0';
			bb = '#004398';
		}
		if (h >= 00 && h <= 4) {
			bg = '#005CD0';
			bb = '#004398';
		}
		$('.header').css({
			'background' : bg,
			'border-bottom' : '5px solid '+bb
		});
	});
	</script>
	
</head>
<body>

	<div class="header">

		<div class="wrapper">

			<h1 class="branding-title" id="branding-title-colors">sniffingDogg</h1>

<!--			<ul class="nav"> 
				<li class="control <?php if ($section == "control") { echo "on"; } ?>"><a href="viewControl.php"><img src=img/books.png></a></li>
				<li class="notify <?php if ($section == "notify") { echo "on"; } ?>"><a href="viewPBSPosted.php"><img src=img/reports.png></a></li>
				<li class="upload <?php if ($section == "upload") { echo "on"; } ?>"><a href="viewCSV.php"><img src=img/csv.png></a></li>
				<li class="settings <?php if ($section == "settings") { echo "on"; } ?>"><a href="viewSettings.php"><img src=img/Tools.png></a></li>
				<li class="contact <?php if ($section == "contact") { echo "on"; } ?>"><a href="viewContact.php"><img src=img/Email.png></a></li>
			</ul>
-->
		</div>

	</div>

	<div id="content">