## Time Machine Webservice Implementation ##

**This is** a working in progress document for the TimeMachine product from SolutionSoft Systems, Inc. 
**Copyright is reserved**.

This document tries to describe the communication protocol between a client and the TimeMachine
web service implementation.  A client can be anything from a web page to a GUI application 
written in donet, Java, etc., or an iOS or Android app.  The communication protocol between 
the management client and the TimeMachine running server is chosen with
the following three objectives in mind:

1. It should be flexible enough to add layers of security protection around; 
2. It should use most recognized technique so that the client implementation can leverage existing techniques; and
3. It should be easy to depoy and maintain.

As such, we decided to choose *HTTP* based web service to facilitate the control data exchange between a managing
client and a running TimeMachine server. All TimeMachine management actions are categorized into the following
groups:

* Adding a relative clock policy
* Adding an absolute clock policy
* Removing a running clock
* Listing running clocks
* Resetting all running clocks
* Listing all TimeMachine feasible users
* Listing all TimeMachine feasible groups

Every action will be mapped into an HTTP GET URL with parameters
and the result will be encoded in JSON format, which will be easy to parse on the client side.

### Assumption ###

TimeMachine web service server is designed to listen on any given TCP port.  For the sake of illustration,
we assume the server will be listen on address 127.0.0.1 at the port 8800.

### Listing all feasible users ###

TimeMachine management server provides an API for listing all feasible users on the runnger server.

For now, we assume all feasible users will be any one other than the super user (uid = 0).

To get the feasible users, one can issue an HTTP GET request using the following URL:

> http://127.0.0.1:8800/tmapp/getusers

If success, the HTTP response code will be 200, and the result will be something like:

> [{"ID":1,"Name":"daemon"},{"ID":2,"Name":"bin"},{"ID":3,"Name":"sys"},
>  {"ID":4,"Name":"adm"},{"ID":5,"Name":"uucp"},{"ID":100,"Name":"guest"},
>  {"ID":9,"Name":"lpd"},{"ID":11,"Name":"lp"},{"ID":6,"Name":"invscout"},
>  {"ID":200,"Name":"snapp"},{"ID":201,"Name":"ipsec"},{"ID":7,"Name":"nuucp"},
>  {"ID":202,"Name":"dradmin"},{"ID":203,"Name":"sshd"},{"ID":20001507,"Name":"u0001507"},
>  {"ID":204,"Name":"tmuser1"},{"ID":205,"Name":"tmuser2"}]

### Listing all feasible groups ###

TimeMachine management server provides an API for listing all feasible groups on the runnger server.

For now, we assume all feasible groups will be any one other than the super group (gid = 0).

To get the feasible groups, one can issue an HTTP GET request using the following URL:

> http://127.0.0.1:8800/tmapp/getgroups

If success, the HTTP response code will be 200, and the result will be something like:

> [{"ID":1,"Name":"staff"},{"ID":2,"Name":"bin"},{"ID":3,"Name":"sys"},
>  {"ID":4,"Name":"adm"},{"ID":5,"Name":"uucp"},{"ID":6,"Name":"mail"},
>  {"ID":7,"Name":"security"},{"ID":8,"Name":"cron"},{"ID":9,"Name":"printq"},
>  {"ID":10,"Name":"audit"},{"ID":28,"Name":"ecs"},{"ID":100,"Name":"usr"},
>  {"ID":20,"Name":"perf"},{"ID":21,"Name":"shutdown"},{"ID":11,"Name":"lp"},
>  {"ID":12,"Name":"invscout"},{"ID":13,"Name":"snapp"},{"ID":200,"Name":"ipsec"},
>  {"ID":14,"Name":"pconsole"},{"ID":201,"Name":"sshd"},{"ID":202,"Name":"tmgroup1"},
>  {"ID":203,"Name":"tmgroup2"}]

### Adding an absolute clock ###

By "absolute clock", one will specify the virtual clock in terms of **yyyymmddHHMM** format,
in the current time zone setting.  For instance, a value of *198505031530* will mean
**May 3, 1985 at 15:30pm**.

#### Adding a user based absolute clock####

For a given user 'foo', one can add an absolute clock *198505031530* with speed at *2* 
through the following URL:
> http://127.0.0.1:8800/tmapp/addabsclock?username=foo&timestamp=198505031530&speed=2

One can also specify the user id using the following URL:
> http://127.0.0.1:8800/tmapp/addabsclock?userid=123&timestamp=198505031530&speed=2

where uid for user 'foo' is 123.

Similar to the usage of the utility 'tmuser', speed parameter can be positive (forwarding), 
negative (backwarding) or zero (frozen).

#### Adding a group based absolute clock ####

For a given group 'bar', one can add an absolute clock *198505031530* with speed at *-1* 
through the following URL:
> http://127.0.0.1:8800/tmapp/addabsclock?groupname=bar&timestamp=198505031530&speed=-1

One can also specify the group id using the following URL:
> http://127.0.0.1:8800/tmapp/addabsclock?groupid=789&timestamp=198505031530&speed=-1

where gid for group 'bar' is 789.

### Adding a relative clock ###

By "relative clock", one will specify the virtual clock with the reference to the current
system clock. Relative clocks can be given as an offset on **year**, **day**, **hour** or **minute**. 
Only one offset should be given.

Assume we want to create a virtual clock by advancing the year with the value 2 and the speed is set to be frozen,
we can add the relative clock using the following URL:
> http://127.0.0.1:8800/tmapp/addrelclock?username=foo&year=2&speed=0

where the user name is 'foo'.

One can replace the username with userid and it will create the virtual clock for the same user. 
An example goes like this:
> http://127.0.0.1:8800/tmapp/addrelclock&userid=123&year=2&speed=0

By replacing the parameter *username* with *groupname*, or *userid* with *groupid*, one can create group
based virtual clocks. In this example, we add a relative clock by decreasing minutes 
by the value of 3600 and setting the speed as -1:

> http://127.0.0.1:8800/tmapp/addrelclock?groupname=bar&minute=3600&speed=-1
> http://127.0.0.1:8800/tmapp/addrelclock&groupid=123&minute=3600&speed=-1

### Listing virtual clock(s) ###

Much like it's counter part in */etc/ssstm/tmuser*, one can list all the running virtual clocks
through the URL:
> http://127.0.0.1:8800/tmapp/listclock

The JSON encoded result is something like:
>[{"username":"tmuser1","timestamp":"20190727204846","speed":0},
> {"timestamp":"20110727205349","speed":2,"groupname":"tmgroup2"}]

or empty if there is none:
> []

### Romoving a running virtual clock ###

To remove a running clock for a given user *foo* with the UID 123, use either of the following URL's:
> http://127.0.0.1:8800/tmapp/removeclock&username=foo
> http://127.0.0.1:8800/tmapp/removeclock&userid=123

To remove a running clock for a given group *bar* with the GID 987, use either of the followign URL's:
> http://127.0.0.1:8800/tmapp/removeclock&groupname=bar
> http://127.0.0.1:8800/tmapp/removeclock&groupid=123

### Error Handling ###

If there is an error in the parameter, format, etc, the web server will return a code *40x* 
with the reason given.

It is adviced that we should always check the return code in case any thing may go wrong.
